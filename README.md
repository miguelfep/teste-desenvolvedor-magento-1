# Teste Desenvolvedor Magento 1

Teste para Vaga de Desenvolvedor Magento 1 na DigitalHub

## Criar um tema com as seguintes especificações (Frontend)

* RWD Child
* Criar CSS base para o tema
* Aplicar o estilo da Newsletter da loja [http://janiero.staging.digitalhub.com.br]

 
## Criar um módulo com as seguintes especificações (Backend):
* Code Pool: Local
* Adicionar os campos **Telefone**, **Nome completo** e **Data de Nascimento** ao formulário de Newsletter encontrado e exibir as informações no grid de listagem das newsletters


## Objetivo
* O módulo deve receber as informações enviadas no formulario e mostra um popup de confirmação!
* Os campos devem ficar disponivel no admin do magento


## Instruções

1. Faça um fork do projeto para sua conta pessoal
2. Crie uma branch com o padrão: `magento-challenge`
3. Coloque seu teste dentro do diretório **[/magento-challenge/seu-nome]
5. Submeta seu código criando um [Pull Request] para `magento-challenge/`

Obs: O teste é aplicado para frontend e backend, execute de acordo com suas habilidades!

Boa Sorte!